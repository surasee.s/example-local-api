package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"localapi/model"
	"log"

	grpcserver "google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
)

func StartStream(client model.GatewayClient) error {
	header := metadata.New(map[string]string{"authorization": "12345"})

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	newCtx := metadata.NewOutgoingContext(ctx, header)

	gatewayClient, err := client.Stream(newCtx)
	if err != nil {
		return err
	}

	for {
		request, err := gatewayClient.Recv()
		// Done listening once client received EOF
		if err == io.EOF {
			log.Println("recv:", "EOF")
			break
		} else if err != nil {
			log.Println("recv:", err.Error())
			break
		}

		// Reset for sending after timeout
		if request.Method == "DELETE" && request.Route == "reset" {
			fmt.Println("reset")
			continue
		}

		bytes, err := json.Marshal(request)
		if err != nil {
			fmt.Println(err.Error())
		}

		fmt.Println(string(bytes))

		response := model.Response{
			RequestId: request.RequestId,
			Body:      `{"code":200,"message":"success","data":"hello"}`,
		}

		err = gatewayClient.Send(&response)
		if err != nil {
			log.Println("send:", err.Error())
		}
	}

	return nil
}

func StartGatewayClient() {
	opts := []grpcserver.DialOption{grpcserver.WithInsecure()}
	conn, err := grpcserver.Dial("127.0.0.1:443", opts...)
	if err != nil {
		panic(err)
	}

	client := model.NewGatewayClient(conn)

	err = StartStream(client)
	if err != nil {
		panic(err)
	}
}

func main() {
	StartGatewayClient()
}
